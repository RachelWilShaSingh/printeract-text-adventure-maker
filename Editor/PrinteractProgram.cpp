#include "PrinteractProgram.hpp"

#include "utilities/Menu.hpp"
#include "utilities/System.hpp"

#include <iostream>
#include <string>
#include <fstream>
using namespace std;

PrinteractProgram::PrinteractProgram()
{
    Load_ProjectList();
}

void PrinteractProgram::Run()
{
    Menu_MainMenu();
}

void PrinteractProgram::Menu_MainMenu()
{
    bool done = false;
    while ( !done )
    {
        Menu::ClearScreen();
        Menu::Header( "MAIN MENU" );

        int choice = Menu::ShowIntMenuWithPrompt( {
        "New Project",
        "Load Project",
        "Quit"
        } );

        if ( choice == 1 )
        {
            Menu_NewProject();
        }
        else if ( choice == 2 )
        {
            Menu_LoadProject();
        }
        else if ( choice == 3 )
        {
            done = true;
        }
    }
}

void PrinteractProgram::Menu_NewProject()
{
    Menu::ClearScreen();
    Menu::Header( "NEW PROJECT" );

    string projectName = Menu::GetStringLine( "What is the project's name?" );
    Project_AddProject( projectName );
}

void PrinteractProgram::Menu_LoadProject()
{
    Menu::ClearScreen();
    Menu::Header( "LOAD PROJECT" );

    int projectIndex = Menu::ShowIntMenuWithPrompt( m_projectList ) - 1; // offset by 1
    m_currentProject = m_projectList[ projectIndex ];
}

void PrinteractProgram::Project_AddProject( string name )
{
    m_projectList.push_back( name );
    Save_ProjectList();
}

void PrinteractProgram::Load_ProjectList()
{
    ifstream input( "projects.data" );

    string projectName;

    while ( getline( input, projectName ) )
    {
        m_projectList.push_back( projectName );
    }
}

void PrinteractProgram::Save_ProjectList()
{
    ofstream output( "projects.data" );

    for ( auto& project : m_projectList )
    {
        output << project << endl;
    }
}
