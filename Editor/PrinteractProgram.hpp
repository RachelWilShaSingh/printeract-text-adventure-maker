#ifndef PRINTERACT_PROGRAM_HPP
#define PRINTERACT_PROGRAM_HPP

#include <vector>
#include <string>
using namespace std;

class PrinteractProgram
{
    public:
    PrinteractProgram();

    void Run();

    private:
    void Save();
    void Load();

    // MENUS
    void Menu_MainMenu();
    void Menu_NewProject();
    void Menu_LoadProject();
    void Menu_MapManager();

    // Functionality
    void Project_AddProject( string name );

    // Saving and loading
    void Load_ProjectList();
    void Save_ProjectList();

    // Members
    vector<string> m_projectList;
    string m_currentProject;
};

#endif
